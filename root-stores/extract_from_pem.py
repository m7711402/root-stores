#!/usr/bin/env python3
"""
Adapted from
https://android.googlesource.com/platform/system/ca-certificates/+/refs/heads/main/google/extract_from_pem.py

Extract certificates from a multi-certificate pem file.
Each certificate in the file is extracted into a format appropriate for use with
Brillo or Android. On success, the contents of the output directory match the
input file exactly. Existing files in the output directory will be deleted.
The current date will be written into the timestamp file, './TIMESTAMP' by
default.
Typical usage (extracting from ./roots.pem and output into ./files):
> ./extract_from_pem.py

it must run next to a roots.pem cert file
"""
import argparse
import datetime
import os
import re
from cryptography import x509
from cryptography.hazmat.primitives import hashes


def WriteCertificateFile(content, base_name, output_dir):
    """Writes a certificate file to the output directory.
    Args:
        content: The file content to write.
        base_name: The file name will be base_name.n where n is the first available
                             non-negative integer. Ex. if myfile.0 exists and has different
                             content, the output file will be myfile.1.
        output_dir: The output directory.
    """
    i = 0
    file_path = os.path.join(output_dir, '%s.%d' % (base_name, i))
    while os.path.exists(file_path):
        with open(file_path) as existing_file:
            if content == existing_file.read():
                # Ignore identical duplicate.
                return
        i += 1
        file_path = os.path.join(output_dir, '%s.%d' % (base_name, i))
    with open(file_path, 'w') as new_file:
        new_file.write(content)


def main():
    output_dir = "files"
    pem_file = "roots.pem"
    if 'y' != input('All files in \'%s\' will be deleted. Proceed? [y,N]: ' %
                                                    output_dir):
        print('Aborted.')
        return
    try:
        for existing_file in os.listdir(output_dir):
            os.remove(os.path.join(output_dir, existing_file))
            os.removedirs(os.path.join(output_dir))
    except:
        pass
    try:
        os.mkdir(os.path.join(output_dir))
    except:
        pass
    with open(pem_file) as pem_file:
        pattern = r'-----BEGIN CERTIFICATE-----[^-]*-----END CERTIFICATE-----'
        pem_certs = re.findall(pattern, pem_file.read())
        for pem_cert in pem_certs:
            cert = x509.load_pem_x509_certificate(str.encode(pem_cert))
            content = pem_cert
            base_name = cert.fingerprint(hashes.SHA256()).hex().upper()
            WriteCertificateFile(content, base_name, output_dir)


if __name__ == '__main__':
    main()
