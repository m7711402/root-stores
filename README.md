# root-stores

## Description
Check how root stores differ from CCADB. Current root stores checked are Chrome, Microsoft and Apple.

## Installation
- Python 3
- Jupyter lab

## Usage
Just run the jupyter notebook

## License
MIT License
